FROM ubuntu:14.04

RUN  apt-get update \
  && apt-get install -y python-dev \
  && apt-get install -y python-pip \
  && apt-get install -y vim \
  && apt-get install -y git \
  && apt-get install -y arduino

RUN pip install platformio

WORKDIR /home/
